---
title: Algoritmia Práctica 1
author:
    - 666151 Barcelona Auría, Federico
    - 700878 Reloba López, Guillermo
---


Introducción
============

Se desea encontrar un algoritmo que dé solución al problema propuesto en la Práctica 1.
Se ha pensado en el siguiente implementación de la solución:

El fichero de entrada de datos ha de seguir el siguiente formato: `x1,x2,t` terminado 
por salto de línea, donde los parámetros simbolizan lo siguiente:

- `x1` y `x2`: Nodos comunicándose
- `t`: Instante de tiempo en el que se comunican.

El usuario introduce los siguientes parámetros al algoritmo: 

1. Fichero de entrada con los datos de conexiones
2. Nodo que se sabe que está infectado
3. Momento en el que se sabe que fué infectado.
4. Nodo del que se quiere comprobar la infección.
5. Momento hasta el que se quiere comprobar la infección.

Corrección y eficiencia
=======================

A partir del parámetro 1, se leen los datos del fichero de entrada, y se ordenan con 
una complejidad de `O(n*log2(n))`.

Utilizando los parámetros 3 y 5, que son los instantes de tiempo límite (mínimo y máximo)
para la realización del algoritmo, primero se filtran los datos provistos para que solo
se tengan en cuenta las conexiones dentro del rango de tiempo límite. 
Complejidad de filtrado de los datos: `O(2*(log2(n)+1)) ~ O(2*log2(n))`

En base a esas conexiones filtradas, se crea el grafo de conexiones entre los nodos, dando lugar
a un grafo más pequeño y permitiendo reducir la búsqueda de conexiones entre nodos.

El algoritmo que detecta la conexión entre dos nodos es básicamente una 
[búsqueda en profundidad](https://en.wikipedia.org/wiki/Depth-first_search).

El coste en tiempo de esta búsqueda es: `O(V+E)` siendo `V` el número de nodos y `E` 
el número de aristas, porque se visitan los nodos sin repetición.

El coste en espacio es: `O(V)` porque se almacenan los nodos sin repetición, en un `std::unordered_set`.

Compilación y ejecución
=======================

Para la compilación se ha utilizado CMake por la facilidad de compilación en múltiples plataformas,
aunque también se ha provisto un fichero Makefile que realmente utiliza CMake por debajo.

Compilar
--------

### Opción 1

Generar con la ayuda de CMake un Makefile para la compilación del proyecto en un directorio a parte: 
```sh
mkdir -p build && cd build
cmake ../
make
```

### Opción 2

Invocar directamente `make build` que ejecuta los 3 comandos anteriores de forma automática.


Ejecutar
--------

Al compilar se generará un ejecutable llamado `alg`. 
Si se ejecuta sin los parámetros correctos, saltará el siguiente mensaje informativo:

```
Usage: ./alg inputFile node1 time1 node2 time2
    inputFile       string  Archivo de entrada donde leer los datos
    node1           int     Nodo infectado
    time1           int     Momento en el que fue infectado
    node2           int     Nodo a comprobar la infeccion
    time2           int     Momento a comprobar el nodo
```

Pruebas
-------

Se han provisto 5 ficheros de prueba con información de conexiones.

- `test/file1`: Primer ejemplo provisto en el documento de la práctica, página 2.
- `test/file2`: Segundo ejemplo provisto en el documento de la práctica, página 2.
- `test/file3`: Ejemplo manual que permite comprobar que si no hay conexión entre los nodos, no hay infección.
- `test/file4`: Ejemplo manual que contiene 10001 nodos y cada uno conecta con el anterior en un instante posterior.
- `test/file5`: ejemplo manual que contiene 10001 nodos y cada uno conecta con el anterior, pero todos en el mismo instante de tiempo.  

### Ejemplo de ejecución

*Dado el log de conexiones 1, se quiere comprobar si el nodo 3 está infectado en el instante 8, sabiendo que el nodo 1 
se ha infectado en el instante 2.*

```
$ ./alg test/file1 1 2 3 8
             
Nodes found in log file: 4
Nodes in the graph to evaluate between time 2 and 8: 4

[Node] id:      3, visited: false, connected with      1 nodes: [ 4 ]
[Node] id:      4, visited: false, connected with      2 nodes: [ 3 2 ]
[Node] id:      1, visited: false, connected with      1 nodes: [ 2 ]
[Node] id:      2, visited: false, connected with      2 nodes: [ 4 1 ]

Are nodes 1 and 3 connected?: Yes

```

Se ha detectado conexión entre los nodos, lo que implica que el nodo 3 también está infectado.