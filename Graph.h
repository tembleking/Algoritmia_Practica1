#ifndef GRAPH_H
#define GRAPH_H

#include <unordered_set>
#include <memory>
#include <ostream>

class Node;

/**
 * Custom structure that allows us to hash and compare the elements
 * from an unordered set even if they are smart pointers, because
 * we will hash and compare its contents rather the pointers themselves.
 */
struct Custom {
    struct Hash {
        std::size_t operator()(std::shared_ptr<Node> const &p) const;

        std::size_t operator()(std::weak_ptr<Node> const &p) const;

        std::size_t operator()(Node const &p) const;
    };

    struct Compare {
        bool operator()(std::shared_ptr<Node> const &a, std::shared_ptr<Node> const &b) const;

        bool operator()(std::weak_ptr<Node> const &a, std::weak_ptr<Node> const &b) const;

        bool operator()(Node const &a, Node const &b) const;
    };
};

/**
 * Node represents a node from the graph, it contains its ID, if it's been
 * visited for the search algorithm and a set of nodes that is connected with.
 */
class Node {
    typedef std::unordered_set<std::weak_ptr<Node>, Custom::Hash, Custom::Compare> WeakSet;
    unsigned int id;
    bool visited;
    WeakSet connectedNodeSet;
public:
    /**
     * @param id ID of the node to create
     */
    explicit Node(unsigned int id);

    /**
     * @param rhs The other node to compare with
     * @return True if the ID of both nodes are the same, false otherwise
     */
    bool operator==(const Node &rhs) const;

    /**
     * @param rhs The other node to compare with
     * @return True if the ID of both nodes are different, false otherwise
     */
    bool operator!=(const Node &rhs) const;

    /**
     * @return ID of the node
     */
    unsigned int getId() const;

    /**
     * Creates a connection with other node.
     * If the connection already exists, ignores it.
     * @param other The other node to create a connection with.
     */
    void addConnectionWith(std::weak_ptr<Node> other);

    /**
     * @return The set of the nodes this node is connected with.
     */
    WeakSet getConnectedNodes() const;

    /**
     * Marks the node as visited for the search algorithm.
     */
    void visit();

    /**
     * @return True if the node has been marked as visited, false otherwise.
     */
    bool isVisited() const;

    friend std::ostream &operator<<(std::ostream &os, const Node &node);
};

/**
 * Graph is the representation of nodes connected with other nodes.
 * The graph can be disconnected.
 * It contains smart pointers for all the nodes that it holds and can
 * find if two nodes are connected.
 */
class Graph {
    typedef std::unordered_set<std::shared_ptr<Node>, Custom::Hash, Custom::Compare> Set;
    Set nodeList;

    /**
     * Looks if a node with an ID exists in the graph and returns it.
     * If the node does not exist in the graph, it creates the node
     * and returns it.
     * @param id ID of the node to retrieve.
     * @return Shared pointer to a node with the provided ID.
     */
    std::shared_ptr<Node> getAndEmplaceNode(unsigned int id);

    /**
     * Algorithm to find a connection between two nodes.
     * It assumes that the nodes exist in the graph.
     * @param a Node to start searching from
     * @param b Node to compare with
     * @return True if there's a connection between the two nodes, false otherwise.
     */
    bool findNodeConnection(std::shared_ptr<Node> a, std::shared_ptr<Node> b);

public:

    /**
     * Creates a connection between two nodes.
     * If the nodes didn't exist in the graph, it creates
     * them by the ID provided and connects them.
     * If the connection between the pair of nodes already exists,
     * this ignores it.
     * @param a ID of the first node
     * @param b ID of the second node
     */
    void addConnection(unsigned int a, unsigned int b);

    /**
     * @return The set of nodes from the graph.
     */
    const Set getNodes() const;

    /**
     * Checks if nodes are connected by their ID.
     * It does not assume that the node exist in the graph.
     * @param a ID of the first node
     * @param b ID of the second node
     * @return True if they are connected, false otherwise.
     */
    bool checkIfConnectedNodes(unsigned int a, unsigned int b);
};

#endif //GRAPH_H
