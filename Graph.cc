#include <iostream>
#include <iomanip>
#include "Graph.h"

// ---------------------- Hashing functions ----------------------------

std::size_t Custom::Hash::operator()(const std::shared_ptr<Node> &p) const {
    return std::hash<unsigned int>()(p->getId());
}

std::size_t Custom::Hash::operator()(const std::weak_ptr<Node> &p) const {
    return std::hash<unsigned int>()(p.lock()->getId());
}

std::size_t Custom::Hash::operator()(const Node &p) const {
    return std::hash<unsigned int>()(p.getId());
}

// ------------------------ Comparing functions -------------------------

bool Custom::Compare::operator()(const std::shared_ptr<Node> &a, const std::shared_ptr<Node> &b) const {
    return *a == *b;
}

bool Custom::Compare::operator()(const std::weak_ptr<Node> &a, const std::weak_ptr<Node> &b) const {
    return *a.lock() == *b.lock();
}

bool Custom::Compare::operator()(Node const &a, Node const &b) const {
    return a == b;
}

// ----------------------------- Node Methods ------------------------------

Node::Node(unsigned int id) : id(id), visited(false) {}

bool Node::operator==(const Node &rhs) const {
    return id == rhs.id;
}

bool Node::operator!=(const Node &rhs) const {
    return !(rhs == *this);
}

void Node::addConnectionWith(std::weak_ptr<Node> other) {
    connectedNodeSet.emplace(other);
}

unsigned int Node::getId() const {
    return id;
}

std::unordered_set<std::weak_ptr<Node>, Custom::Hash, Custom::Compare> Node::getConnectedNodes() const {
    return connectedNodeSet;
}

std::ostream &operator<<(std::ostream &os, const Node &node) {
    os << "[Node] id: " << std::setw(6) << node.id << ", visited: " << std::setw(5) << ((node.visited) ? "true" : "false") << ", connected with "
       << std::setw(6) << node.connectedNodeSet.size() << " nodes: [ ";

    for (const std::weak_ptr<Node> &connectedNode : node.connectedNodeSet) {
        auto ptr = connectedNode.lock();
        if (ptr) os << ptr->getId() << " ";
    }

    os << "]";
    return os;
}

void Node::visit() {
    visited = true;
}

bool Node::isVisited() const {
    return visited;
}

// ----------------------  Graph Methods ----------------------------

void Graph::addConnection(unsigned int a, unsigned int b) {
    auto aPtr = getAndEmplaceNode(a);
    auto bPtr = getAndEmplaceNode(b);

    aPtr->addConnectionWith(bPtr);
    bPtr->addConnectionWith(aPtr);
}

std::shared_ptr<Node> Graph::getAndEmplaceNode(unsigned int id) {
    auto pointer = std::make_shared<Node>(id);
    auto iterator = nodeList.find(pointer);

    if (iterator != nodeList.end()) {
        pointer = *iterator;
    } else {
        nodeList.emplace(pointer);
    }

    return pointer;
}

const std::unordered_set<std::shared_ptr<Node>, Custom::Hash, Custom::Compare> Graph::getNodes() const {
    return nodeList;
}

bool Graph::checkIfConnectedNodes(unsigned int a, unsigned int b) {
    auto aItr = nodeList.find(std::make_shared<Node>(a));
    auto bItr = nodeList.find(std::make_shared<Node>(b));
    auto endItr = nodeList.end();

    if (aItr == endItr || bItr == endItr) return false; // Not found nodes a or b, therefore, they can't be connected.

    return findNodeConnection(*aItr, *bItr);
}

bool Graph::findNodeConnection(std::shared_ptr<Node> a, std::shared_ptr<Node> b) {
    if (a->isVisited()) return false; // skip if the node was already visited

    if (*a == *b) return true; // if they are equal, they are obviously connected

    a->visit(); // mark the first node as visited

    for (auto node : a->getConnectedNodes()) {
        if (findNodeConnection(node.lock(), b)) return true; // recursive iteration
    }

    return false; // not found a connection
}
