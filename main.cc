#include <iostream>
#include <vector>
#include <fstream>
#include <memory>
#include <unordered_set>

#include "usage.h"
#include "Edge.h"
#include "fileParser.h"
#include "Graph.h"


int main(int argc, char **argv) {

    if (argc != 6) {
        usage(argv[0]);
        exit(2);
    }

    int nodeInfected = std::atoi(argv[2]);
    int timeInfected = std::atoi(argv[3]);
    int nodeToCheck = std::atoi(argv[4]);
    int timeToCheck = std::atoi(argv[5]);

    // Open the file and parse it generating a vector of edges
    std::ifstream file(argv[1], std::ios_base::in);
    std::vector<Edge> vector = parseStream(file);
    sortVector(vector);

    // Find edges that are connected with times between timeInfected and timeToCheck
    std::cout << "Nodes found in log file: " << vector.size() << std::endl;
    auto bounds = findBounds(vector, timeInfected, timeToCheck);

    // Fill the graph of connections
    Graph graph;
    for (; bounds.first != bounds.second; bounds.first++) {
        graph.addConnection(bounds.first->getNode1(), bounds.first->getNode2());
    }

    auto graphNodeSet = graph.getNodes();

    std::cout
            << "Nodes in the graph to evaluate between time "
            << timeInfected << " and " << timeToCheck << ": " << graphNodeSet.size()
            << std::endl << std::endl;

    // Just print the nodes in the graph (not part of the algorithm)
    for (const std::shared_ptr<Node> &item : graphNodeSet) {
        std::cout << *item << std::endl;
    }

    std::cout << std::endl;

    // Check if the infected node has connected with the node to check.
    auto areConnected = graph.checkIfConnectedNodes(nodeInfected, nodeToCheck);
    std::cout
            << "Are nodes " << nodeInfected << " and " << nodeToCheck << " connected?: "
            << (areConnected ? "Yes" : "No")
            << std::endl;

    return (areConnected ? 1 : 0);
}
