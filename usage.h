#ifndef USAGE_H
#define USAGE_H

#include <iostream>

void usage(const char *programName) {
    std::cout
            << "Usage: " << programName << " inputFile node1 time1 node2 time2" << std::endl
            << "    inputFile       string  Archivo de entrada donde leer los datos" << std::endl
            << "    node1           int     Nodo infectado" << std::endl
            << "    time1           int     Momento en el que fue infectado" << std::endl
            << "    node2           int     Nodo a comprobar la infeccion" << std::endl
            << "    time2           int     Momento a comprobar el nodo" << std::endl;
}

#endif //USAGE_H
