#ifndef FILEPARSER_H
#define FILEPARSER_H

#include <vector>
#include <sstream>
#include <algorithm>
#include "Edge.h"

/**
 * Parsea un stream de entrada, creando una lista de ConnectionLog. <br>
 * Para ello es necesario que el formato del contenido del stream sea del tipo: <br>
 *    x1,x2,t <br>
 * Donde los elementos x1 y x2 son primer y segundo nodo que han conectado y t es el
 * momento en el tiempo cuando han conectado. <br>
 * Coste: O(N*log_2(N))
 * @param filename Archivo que contiene las conexiones
 * @return Vector de ConnectionLog que representan las conexiones ordenadas por tiempo
 */
std::vector<Edge> parseStream(std::istream &istream) {
    std::vector<Edge> logs;
    for (std::string line; std::getline(istream, line);)  // O(N)
    {
        std::stringstream lineStream(line);
        std::string x1, x2, t;
        std::getline(lineStream, x1, ',');
        std::getline(lineStream, x2, ',');
        std::getline(lineStream, t, ',');

        logs.emplace_back(std::atoi(x1.c_str()), std::atoi(x2.c_str()), std::atoi(t.c_str()));
    }
    return logs;
}


struct EdgeAndIntComparator {
    bool operator()(const Edge &a, const Edge &b) {
        return a.getTime() < b.getTime();
    }

    bool operator()(const Edge &a, int b) {
        return a.getTime() < b;
    }

    bool operator()(int a, const Edge &b) {
        return a < b.getTime();
    }
};

auto findBounds(const std::vector<Edge> &vector, int minTime, int maxTime) {
    return std::make_pair(std::lower_bound(vector.begin(), vector.end(), minTime, EdgeAndIntComparator()),
                          std::upper_bound(vector.begin(), vector.end(), maxTime, EdgeAndIntComparator()));
}

auto sortVector(std::vector<Edge> &vector) {
    std::sort(vector.begin(), vector.end()); // O(N*log_2(N))
}

#endif //FILEPARSER_H
