#ifndef EDGES_H
#define EDGES_H


#include <ostream>

class Edge {
    unsigned int node1;
    unsigned int node2;
    unsigned int time;

public:
    Edge(unsigned int node1, unsigned int node2, unsigned int time) : node1(node1), node2(node2), time(time) {}

    int getTime() const {
        return time;
    }

    unsigned int getNode1() const {
        return node1;
    }

    unsigned int getNode2() const {
        return node2;
    }

    bool operator==(const Edge &rhs) const {
        return time == rhs.time;
    }

    bool operator!=(const Edge &rhs) const {
        return !(rhs == *this);
    }

    bool operator<(const Edge &rhs) const {
        return time < rhs.time;
    }

    bool operator>(const Edge &rhs) const {
        return rhs < *this;
    }

    bool operator<=(const Edge &rhs) const {
        return !(rhs < *this);
    }

    bool operator>=(const Edge &rhs) const {
        return !(*this < rhs);
    }

    friend std::ostream &operator<<(std::ostream &os, const Edge &edge) {
        os << "[Edge] node1: " << edge.node1 << " node2: " << edge.node2 << " time: " << edge.time;
        return os;
    }
};


#endif //EDGES_H
