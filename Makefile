build:
	mkdir -p ./build
	cd ./build && cmake .. && make
	mv ./build/alg .

clean:
	rm -rf ./build
	rm alg

tar:
	tar -cvf practica1.tar *.h *.cc CMakeLists.txt Makefile README.md test Memoria.pdf